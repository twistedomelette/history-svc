FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/gitlab.com/tokend/history-svc
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/history-svc /go/src/gitlab.com/tokend/history-svc


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/history-svc /usr/local/bin/history-svc
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["history-svc"]
