package handlers

import (
	"gitlab.com/tokend/history-svc/internal/data"
	"net/http"

	"gitlab.com/tokend/go/doorman"
	"gitlab.com/tokend/history-svc/internal/service/requests"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func CreateHistoryEvent(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewHistoryEventRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	constraints := []doorman.SignerConstraint{
		doorman.SignatureOf(HorizonInfo(r).Attributes.MasterAccountId),
		doorman.SignatureOf(request.Data.Attributes.Sender),
	}

	if Doorman(r, constraints...) != nil {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	err = HistoryQ(r).Create(data.History{
		Id:              request.Data.ID,
		CurrencyAmount:  request.Data.Attributes.CurrencyAmount,
		CurrencyPayment: request.Data.Attributes.CurrencyPayment,
		Recipient:       request.Data.Attributes.Recipient,
		Sender:          request.Data.Attributes.Sender,
		Status:          request.Data.Attributes.Status,
		TokenId:         request.Data.Attributes.TokenId,
		Type:            request.Data.Attributes.Type,
		UsdAmount:       request.Data.Attributes.UsdAmount,
	})

	if err != nil {
		Log(r).WithError(err).Error("failed to create history event")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ape.Render(w, http.StatusNoContent)
}
