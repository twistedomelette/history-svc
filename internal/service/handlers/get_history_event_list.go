package handlers

import (
	"net/http"

	"gitlab.com/tokend/go/doorman"
	"gitlab.com/tokend/history-svc/internal/service/requests"
	"gitlab.com/tokend/history-svc/resources"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func GetHistoryEventList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetEventListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	q := HistoryQ(r)
	q = q.Page(request.OffsetPageParams)

	constraints := []doorman.SignerConstraint{
		doorman.SignatureOf(HorizonInfo(r).Attributes.MasterAccountId),
	}

	if request.Statuses != nil {
		q = q.FilterStatuses(request.Statuses)
	}
	if request.Sender != nil {
		constraints = append(constraints, doorman.SignatureOf(*request.Sender))
		q = q.FilterSender(*request.Sender)
	}

	if Doorman(r, constraints...) != nil {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	events, err := q.Select()
	if err != nil {
		Log(r).WithError(err).Error("failed to get events from DB")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.HistoryListRequest{
		Data:  make([]resources.History, 0, len(events)),
		Links: getLinks(request, r, Log(r)),
	}

	for _, event := range events {
		response.Data = append(response.Data, event.Resource())
	}

	ape.Render(w, response)
}
