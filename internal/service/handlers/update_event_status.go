package handlers

import (
	"net/http"

	"gitlab.com/tokend/go/signcontrol"
	"gitlab.com/tokend/history-svc/internal/data"
	"gitlab.com/tokend/history-svc/internal/service/helpers"
	"gitlab.com/tokend/history-svc/internal/service/requests"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"github.com/pkg/errors"
)

func UpdateEventStatus(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewEventStatusRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	historyQ := HistoryQ(r)

	accountID, err := signcontrol.CheckSignature(r)

	if err != nil {
		if errors.Is(err, signcontrol.ErrNotSigned) {
			Log(r).WithError(err).Error("failed to get sign id")
			ape.RenderErr(w, problems.Unauthorized())
			return
		}
		Log(r).WithError(err).Error("failed to get sign id")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	err = historyQ.Transaction(func(q data.HistoryQ) error {
		err = q.SetStatus(request.Data.Attributes.Ids, request.Data.Attributes.Status, accountID)
		if err != nil {
			return errors.Wrap(err, "failed to set status")
		}

		return nil
	})

	if err != nil {
		if errors.Is(err, helpers.ErrNotMatchEventIds) {
			Log(r).WithError(err).Error("failed to process pending transaction")
			ape.RenderErr(w, problems.BadRequest(err)...)
			return
		}
		Log(r).WithError(err).Error("failed to process pending transaction")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ape.Render(w, http.StatusNoContent)
}
