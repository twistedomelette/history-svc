package handlers

import (
	"net/http"

	"gitlab.com/tokend/history-svc/resources"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/urlval"
)

type ListRequest interface {
	NextPage()
}

// getLinks returns nil if error occurred
func getLinks(request ListRequest, r *http.Request, log *logan.Entry) *resources.Links {
	self, err := urlval.Encode(request)
	if err != nil {
		log.WithError(err).Error("failed to encode url value")
		return nil
	}

	request.NextPage()
	next, err := urlval.Encode(request)
	if err != nil {
		log.WithError(err).Error("failed to encode url value")
		return nil
	}

	next = r.URL.Path + "?" + next
	self = r.URL.Path + "?" + self

	return &resources.Links{
		First: "",
		Last:  "",
		Next:  next,
		Prev:  "",
		Self:  self,
	}
}
