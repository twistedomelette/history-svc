package handlers

import (
	"context"
	"net/http"

	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/go/doorman"
	"gitlab.com/tokend/history-svc/internal/data"

	"gitlab.com/distributed_lab/logan/v3"
	regources "gitlab.com/tokend/regources/generated"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	historyQKey
	horizonInfoCtxKey
	keyDoorman
	ownerCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxHistoryQ(entry data.HistoryQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, historyQKey, entry)
	}
}

func HistoryQ(r *http.Request) data.HistoryQ {
	return r.Context().Value(historyQKey).(data.HistoryQ).New()
}

func CtxOwner(keys keyer.Keys) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, ownerCtxKey, keys)
	}
}

func CtxHorizonInfo(entry *regources.HorizonState) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, horizonInfoCtxKey, entry)
	}
}

func HorizonInfo(r *http.Request) *regources.HorizonState {
	return r.Context().Value(horizonInfoCtxKey).(*regources.HorizonState)
}

func CtxDoorman(d doorman.Doorman) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, keyDoorman, d)
	}
}

func Doorman(r *http.Request, constraints ...doorman.SignerConstraint) error {
	d := r.Context().Value(keyDoorman).(doorman.Doorman)
	return d.Check(r, constraints...)
}
