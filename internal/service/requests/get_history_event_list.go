package requests

import (
	"net/http"

	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/urlval"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type GetHistoryEvent struct {
	pgdb.OffsetPageParams
	Sender   *string  `filter:"sender"`
	Statuses []string `filter:"statuses"`
}

func NewGetEventListRequest(r *http.Request) (GetHistoryEvent, error) {
	var request GetHistoryEvent

	err := urlval.Decode(r.URL.Query(), &request)
	if err != nil {
		return request, err
	}

	return request, request.Validate()
}

func (r GetHistoryEvent) NextPage() {
	r.PageNumber++
}

func (r GetHistoryEvent) Validate() error {
	verrors := validation.Errors{
		"page[order]": validation.Validate(r.Order, validation.In(pgdb.OrderTypeAsc, pgdb.OrderTypeDesc)),
	}

	return verrors.Filter()
}
