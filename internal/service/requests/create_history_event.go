package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/tokend/history-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func NewHistoryEventRequest(r *http.Request) (resources.HistoryRequest, error) {
	var request resources.HistoryRequest

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{
			"/": errors.Wrap(err, "failed to decode body"),
		}
	}

	return request, ValidateHistoryRequest(request)
}

func ValidateHistoryRequest(r resources.HistoryRequest) error {
	return validation.Errors{
		"status":   validation.Validate(r.Data.Attributes.Status, validation.Required),
		"sender":   validation.Validate(r.Data.Attributes.Sender, validation.Required),
		"token_id": validation.Validate(r.Data.Attributes.TokenId, validation.Required),
		"type":     validation.Validate(r.Data.Attributes.Type, validation.Required),
	}.Filter()
}
