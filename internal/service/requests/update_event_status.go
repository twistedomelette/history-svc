package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/tokend/history-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func NewEventStatusRequest(r *http.Request) (resources.EventIdsResponse, error) {
	var request resources.EventIdsResponse

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		return request, validation.Errors{
			"/": errors.Wrap(err, "failed to decode body"),
		}
	}

	return request, ValidateEventStatusRequest(request)
}

func ValidateEventStatusRequest(r resources.EventIdsResponse) error {
	return validation.Errors{
		"status": validation.Validate(r.Data.Attributes.Status, validation.Required),
	}.Filter()
}
