package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/doorman"
	"gitlab.com/tokend/history-svc/internal/config"
	"gitlab.com/tokend/history-svc/internal/data/postgres"
	"gitlab.com/tokend/history-svc/internal/horizon"
	"gitlab.com/tokend/history-svc/internal/service/handlers"
)

func (s *service) router(cfg config.Config) chi.Router {
	info, err := cfg.HorizonConnector().Info()
	if err != nil {
		panic(errors.Wrap(err, "failed to get horizon info"))
	}
	accountQ := horizon.NewConnector(cfg.Keys(), cfg.Log(), cfg.Client())

	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxHistoryQ(postgres.NewHistoryQ(cfg.DB())),
			handlers.CtxOwner(cfg.Keys()),
			handlers.CtxDoorman(doorman.New(
				cfg.Doorman().SkipSignCheck,
				accountQ),
			),
			handlers.CtxHorizonInfo(&info),
		),
	)
	r.Route("/integrations/history-svc", func(r chi.Router) {
		r.Post("/events", handlers.CreateHistoryEvent)
		r.Patch("/completion", handlers.UpdateEventStatus)
		r.Get("/events", handlers.GetHistoryEventList)
	})

	return r
}
