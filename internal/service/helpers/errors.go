package helpers

import "gitlab.com/distributed_lab/logan/v3/errors"

var (
	ErrNotMatchEventIds = errors.New("affected rows does not match the number of history event id")
)
