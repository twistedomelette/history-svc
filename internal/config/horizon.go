package config

import (
	"net/http"
	"net/url"

	"gitlab.com/tokend/history-svc/internal/horizon"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	abstract "gitlab.com/tokend/connectors/horizon"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type HorizonConfig struct {
	Endpoint *url.URL     `fig:"endpoint"`
	Signer   keypair.Full `fig:"signer,required"`
}

func (c *config) HorizonConnector() *horizon.Connector {
	return c.horizonConnector.Do(func() interface{} {
		var config HorizonConfig

		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "horizon")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out horizon"))
		}

		cli := signed.NewClient(http.DefaultClient, config.Endpoint).WithSigner(config.Signer)
		connector := abstract.NewConnector(cli, config.Signer)
		if connector == nil {
			panic("connector is nil")
		}

		return horizon.NewConnector(c.Keys(), c.Log(), c.Client())
	}).(*horizon.Connector)
}
