package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type Doorman struct {
	SkipSignCheck bool `fig:"skip_sign_check,required"`
	CacheSigners  bool `fig:"cache_signers"`
}

type Doormaner interface {
	Doorman() Doorman
}

type doormaner struct {
	once   comfig.Once
	getter kv.Getter
}

func NewDoormaner(getter kv.Getter) Doormaner {
	return &doormaner{
		getter: getter,
	}
}

func (k *doormaner) Doorman() Doorman {
	return k.once.Do(func() interface{} {
		var doorman Doorman

		err := figure.
			Out(&doorman).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(k.getter, "doorman")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out doorman"))
		}

		return doorman
	}).(Doorman)
}
