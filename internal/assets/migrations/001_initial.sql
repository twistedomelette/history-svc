-- +migrate Up

CREATE TYPE history_status AS ENUM ('pending', 'accepted', 'rejected');

CREATE TABLE history_event (
    id               bigserial not null primary key unique,
    status           history_status default 'pending',
    type             smallint not null,
    token_id         bigint not null,
    sender           char(56) not null,
    recipient        char(56) default '',
    usd_amount       bigint default 0,
    currency_payment text default '',
    currency_amount  bigint default 0
);

CREATE INDEX sender_idx ON history_status(sender);

-- +migrate Down

drop table history_event;
drop type history_status;
drop index sender_idx;

