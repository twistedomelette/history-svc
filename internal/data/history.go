package data

import (
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/history-svc/resources"
)

type History struct {
	Id              string `db:"id"`
	CurrencyAmount  uint64 `db:"currency_amount" structs:"currency_amount"`
	CurrencyPayment string `db:"currency_payment" structs:"currency_payment"`
	Recipient       string `db:"recipient" structs:"recipient"`
	Sender          string `db:"sender" structs:"sender"`
	Status          string `db:"status" structs:"status"`
	TokenId         int64  `db:"token_id" structs:"token_id"`
	Type            int8   `db:"type" structs:"type"`
	UsdAmount       uint64 `db:"usd_amount" structs:"usd_amount"`
}

type HistoryQ interface {
	New() HistoryQ

	FilterSender(sender string) HistoryQ
	FilterStatuses(statuses []string) HistoryQ

	Create(historyEvent History) error
	SetStatus(ids []int64, status string, accountID string) error
	Select() ([]History, error)

	Transaction(fn func(q HistoryQ) error) error
	Page(params pgdb.OffsetPageParams) HistoryQ
}

func (h History) Resource() resources.History {
	historyEvent := resources.History{
		Key: resources.NewKeyString(h.Id, resources.HISTORY),
		Attributes: resources.HistoryAttributes{
			Status:          h.Status,
			Type:            h.Type,
			TokenId:         h.TokenId,
			Sender:          h.Sender,
			Recipient:       h.Recipient,
			UsdAmount:       h.UsdAmount,
			CurrencyPayment: h.CurrencyPayment,
			CurrencyAmount:  h.CurrencyAmount,
		},
	}

	return historyEvent
}
