package postgres

import (
	"gitlab.com/tokend/history-svc/internal/data"
	"gitlab.com/tokend/history-svc/internal/service/helpers"

	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
)

const (
	historyEventTable = "history_event"
)

var historyEventsColumns = []string{"id", "status", "type", "token_id", "sender", "recipient", "usd_amount", "currency_payment", "currency_amount"}

type HistoryQ struct {
	db       *pgdb.DB
	selector sq.SelectBuilder
}

func NewHistoryQ(db *pgdb.DB) data.HistoryQ {
	return &HistoryQ{
		db:       db.Clone(),
		selector: sq.Select(historyEventsColumns...).From(historyEventTable),
	}
}

func (q *HistoryQ) New() data.HistoryQ {
	return NewHistoryQ(q.db)
}

func (q *HistoryQ) Create(historyEvent data.History) error {
	stmt := sq.
		Insert(historyEventTable).
		SetMap(structs.Map(historyEvent))

	return q.db.Exec(stmt)
}

func (q *HistoryQ) Transaction(fn func(q data.HistoryQ) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *HistoryQ) SetStatus(ids []int64, status string, accountID string) error {
	stmt := sq.Update(historyEventTable).
		Set("status", status).
		Where(sq.Eq{
			"id":     ids,
			"sender": accountID,
		})
	res, err := q.db.ExecWithResult(stmt)
	if err != nil {
		return errors.Wrap(err, "unable to update row")
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "unable to get affected rows")
	}
	if rowsAffected != int64(len(ids)) {
		return helpers.ErrNotMatchEventIds
	}
	return nil
}

func (q *HistoryQ) Select() ([]data.History, error) {
	var result []data.History
	err := q.db.Select(&result, q.selector)
	return result, err
}

func (q *HistoryQ) FilterSender(sender string) data.HistoryQ {
	q.selector = q.selector.Where(sq.Eq{
		"sender": sender,
	})
	return q
}

func (q *HistoryQ) FilterStatuses(statuses []string) data.HistoryQ {
	q.selector = q.selector.Where(sq.Eq{
		"status": statuses,
	})
	return q
}

func (q *HistoryQ) Page(params pgdb.OffsetPageParams) data.HistoryQ {
	q.selector = params.ApplyTo(q.selector, historyEventsColumns[0])
	return q
}
