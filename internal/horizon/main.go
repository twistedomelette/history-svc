package horizon

import (
	"net/url"

	"github.com/eko/gocache/marshaler"

	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/json-api-connector/client"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/lazyinfo"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"
	regources "gitlab.com/tokend/regources/generated"
)

type Connector struct {
	log *logan.Entry

	*horizon.Connector
	*horizon.Streamer
	*lazyinfo.LazyInfoer

	cli client.Client

	signer keypair.Full

	handySignersCache *marshaler.Marshaler
}

func NewConnector(keys keyer.Keys, log *logan.Entry, cli client.Client) *Connector {
	return &Connector{
		Connector:  horizon.NewConnector(cli),
		LazyInfoer: lazyinfo.New(cli),
		signer:     keys.Signer,
		cli:        cli,
		log:        log,
	}
}

func (c *Connector) TxBuilder() (*xdrbuild.Builder, error) {
	state, err := c.State()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get network passphrase and tx expiration period")
	}

	return xdrbuild.NewBuilder(state.Data.Attributes.NetworkPassphrase, state.Data.Attributes.TxExpirationPeriod), nil
}

func (c *Connector) State() (*regources.HorizonStateResponse, error) {
	result := &regources.HorizonStateResponse{}
	infoUrl, _ := url.Parse("/v3/info")

	err := c.Get(infoUrl, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get horizon state")
	}

	return result, nil
}

func (c *Connector) List(u string, params horizon.Encoder) *horizon.Streamer {
	return horizon.NewStreamer(c.cli, u, params)
}
