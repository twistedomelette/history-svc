package horizon

import (
	"fmt"
	"time"

	"github.com/eko/gocache/store"
	"github.com/go-redis/redis/v8"
	"github.com/spf13/cast"

	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/resources"
	regources "gitlab.com/tokend/regources/generated"
)

// Signer has same struct as resources.Signer (used in SignersQ by default) but with additional msgpack tags to handle unmarshaling from cache
type Signer struct {
	PublicKey string `msgpack:"public_key"`
	AccountID string `msgpack:"account_id"`
	Weight    int    `msgpack:"weight"`
	Role      uint64 `msgpack:"role"`
	Identity  uint32 `msgpack:"identity"`
}

func (c *Connector) Signers(address string) ([]resources.Signer, error) {
	if c.handySignersCache == nil {
		signers, err := c.getSignersSlow(address)
		if err != nil {
			return nil, err
		}
		return rawToResources(*signers), nil
	}

	var cachedSigners []Signer
	if _, err := c.handySignersCache.Get(address, &cachedSigners); !isNotFoundErr(err) {
		if len(cachedSigners) != 0 {
			return cachedToResources(cachedSigners), nil
		}
		c.log.WithError(err).Error("failed to get cached signers slice value, falling back to getting from horizon")
	}

	signersList, err := c.getSignersSlow(address)
	if err != nil {
		return nil, err
	}

	err = c.handySignersCache.Set(address, rawToCacheable(*signersList), &store.Options{
		Expiration: 5 * time.Second,
	})
	if err != nil {
		c.log.WithError(err).Error("failed to cache signers slice value, next time will try to get from horizon")
	}

	return rawToResources(*signersList), nil
}

func (c *Connector) getSignersSlow(acctAddress string) (*regources.SignerListResponse, error) {
	var res regources.SignerListResponse

	err := c.List(fmt.Sprintf("/v3/accounts/%s/signers", acctAddress), connector.StubEncoder{}).Next(&res)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get signers from horizon")
	}

	return &res, nil // fixme return &res, err
}

func rawToResources(signerList regources.SignerListResponse) []resources.Signer {
	res := make([]resources.Signer, 0, len(signerList.Data))
	for _, raw := range signerList.Data {
		res = append(res, resources.Signer{
			AccountID: raw.ID,
			Weight:    cast.ToInt(raw.Attributes.Weight),
			Identity:  raw.Attributes.Identity,
		})
	}
	return res
}

func rawToCacheable(signerList regources.SignerListResponse) []Signer {
	res := make([]Signer, 0, len(signerList.Data))
	for _, raw := range signerList.Data {
		res = append(res, Signer{
			AccountID: raw.ID,
			Weight:    cast.ToInt(raw.Attributes.Weight),
			Identity:  raw.Attributes.Identity,
		})
	}
	return res
}

func cachedToResources(signers []Signer) []resources.Signer {
	res := make([]resources.Signer, 0, len(signers))
	for _, raw := range signers {
		res = append(res, resources.Signer{
			PublicKey: raw.PublicKey,
			AccountID: raw.AccountID,
			Weight:    raw.Weight,
			Role:      raw.Role,
			Identity:  raw.Identity,
		})
	}
	return res
}

func isNotFoundErr(err error) bool {
	return err == redis.Nil
}
