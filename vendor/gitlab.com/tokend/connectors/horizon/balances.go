package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const balancesUrl = "/v3/balances"

func (c *Connector) Balances(query ...Query) ([]regources.Balance, *regources.Included, error) {
	var response regources.BalanceListResponse

	err := c.GetList(balancesUrl, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.Balance{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting balances")
	}

	return response.Data, &response.Included, err
}

func (c *Connector) BalanceById(id string, query ...Query) (*regources.Balance, *regources.Included, error) {
	var response regources.BalanceResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: balancesUrl,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting balance")
	}

	return &response.Data, &response.Included, err
}
