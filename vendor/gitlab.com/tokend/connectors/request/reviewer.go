package request

import (
	"context"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/json-api-connector/client"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/submit"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	regources "gitlab.com/tokend/regources/generated"
)

type ReviewDetails struct {
	TasksToAdd      uint32
	TasksToRemove   uint32
	ExternalDetails string
	RejectReason    string
}

type Reviewer struct {
	*submit.Submitter
	Keys keyer.Keys
}

func New(client client.Client, keys keyer.Keys) *Reviewer {
	return &Reviewer{
		Submitter: submit.New(client),
		Keys:      keys,
	}
}

type ReviewMsg struct {
	Request         regources.ReviewableRequest
	ReviewDetails   ReviewDetails
	ExternalDetails xdrbuild.ReviewRequestDetailsProvider
}

func (s *Reviewer) PermanentRejectMultiple(ctx context.Context, msgs ...ReviewMsg) ([]xdr.ReviewRequestResult, error) {
	builder, err := s.TXBuilder()
	if err != nil {
		return nil, errors.Wrap(err, "error getting builder")
	}

	tx := builder.Transaction(s.Keys.Source)

	for _, msg := range msgs {
		id := cast.ToUint64(msg.Request.ID)
		requestHash := msg.Request.Attributes.Hash

		tx = tx.Op(&xdrbuild.ReviewRequest{
			ID:      id,
			Hash:    &requestHash,
			Action:  xdr.ReviewRequestOpActionPermanentReject,
			Reason:  msg.ReviewDetails.RejectReason,
			Details: msg.ExternalDetails,
			ReviewDetails: xdrbuild.ReviewDetails{
				TasksToAdd:      msg.ReviewDetails.TasksToAdd,
				TasksToRemove:   msg.ReviewDetails.TasksToRemove,
				ExternalDetails: msg.ReviewDetails.ExternalDetails,
			},
		})
	}

	tx = tx.Sign(s.Keys.Signer)

	return s.submitMultipleReviewsTx(ctx, tx)
}

func (s *Reviewer) ApproveMultiple(ctx context.Context, msgs ...ReviewMsg) ([]xdr.ReviewRequestResult, error) {
	builder, err := s.TXBuilder()
	if err != nil {
		return nil, errors.Wrap(err, "error getting builder")
	}

	tx := builder.Transaction(s.Keys.Source)

	for _, msg := range msgs {
		id := cast.ToUint64(msg.Request.ID)
		requestHash := msg.Request.Attributes.Hash

		tx = tx.Op(&xdrbuild.ReviewRequest{
			ID:      id,
			Hash:    &requestHash,
			Action:  xdr.ReviewRequestOpActionApprove,
			Reason:  msg.ReviewDetails.RejectReason,
			Details: msg.ExternalDetails,
			ReviewDetails: xdrbuild.ReviewDetails{
				TasksToAdd:      msg.ReviewDetails.TasksToAdd,
				TasksToRemove:   msg.ReviewDetails.TasksToRemove,
				ExternalDetails: msg.ReviewDetails.ExternalDetails,
			},
		})
	}

	tx = tx.Sign(s.Keys.Signer)

	return s.submitMultipleReviewsTx(ctx, tx)
}

func (s *Reviewer) Approve(ctx context.Context, request regources.ReviewableRequest, reviewDetails ReviewDetails,
	externalDetails xdrbuild.ReviewRequestDetailsProvider) (*xdr.ReviewRequestResult, error) {
	id := cast.ToUint64(request.ID)

	builder, err := s.TXBuilder()
	if err != nil {
		return nil, errors.Wrap(err, "error getting builder")
	}

	tx := builder.Transaction(s.Keys.Source).Op(&xdrbuild.ReviewRequest{
		ID:      id,
		Hash:    &request.Attributes.Hash,
		Action:  xdr.ReviewRequestOpActionApprove,
		Reason:  reviewDetails.RejectReason,
		Details: externalDetails,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      reviewDetails.TasksToAdd,
			TasksToRemove:   reviewDetails.TasksToRemove,
			ExternalDetails: reviewDetails.ExternalDetails,
		},
	}).Sign(s.Keys.Signer)

	return s.submitTransaction(ctx, tx)
}

func (s *Reviewer) Reject(ctx context.Context,
	request regources.ReviewableRequest,
	reviewDetails ReviewDetails,
	externalDetails xdrbuild.ReviewRequestDetailsProvider) (*xdr.ReviewRequestResult, error) {
	id := cast.ToUint64(request.ID)

	builder, err := s.TXBuilder()
	if err != nil {
		return nil, errors.Wrap(err, "error getting builder")
	}

	tx := builder.Transaction(s.Keys.Source).Op(&xdrbuild.ReviewRequest{
		ID:      id,
		Hash:    &request.Attributes.Hash,
		Action:  xdr.ReviewRequestOpActionReject,
		Reason:  reviewDetails.RejectReason,
		Details: externalDetails,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      reviewDetails.TasksToAdd,
			TasksToRemove:   reviewDetails.TasksToRemove,
			ExternalDetails: reviewDetails.ExternalDetails,
		},
	}).Sign(s.Keys.Signer)

	return s.submitTransaction(ctx, tx)
}

func (s *Reviewer) PermanentReject(ctx context.Context,
	request regources.ReviewableRequest,
	reviewDetails ReviewDetails,
	externalDetails xdrbuild.ReviewRequestDetailsProvider) (*xdr.ReviewRequestResult, error) {
	id := cast.ToUint64(request.ID)

	builder, err := s.TXBuilder()
	if err != nil {
		return nil, errors.Wrap(err, "error getting builder")
	}

	tx := builder.Transaction(s.Keys.Source).Op(&xdrbuild.ReviewRequest{
		ID:      id,
		Hash:    &request.Attributes.Hash,
		Action:  xdr.ReviewRequestOpActionPermanentReject,
		Reason:  reviewDetails.RejectReason,
		Details: externalDetails,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      reviewDetails.TasksToAdd,
			TasksToRemove:   reviewDetails.TasksToRemove,
			ExternalDetails: reviewDetails.ExternalDetails,
		},
	}).Sign(s.Keys.Signer)

	return s.submitTransaction(ctx, tx)
}

func (s *Reviewer) submitTransaction(ctx context.Context, tx *xdrbuild.Transaction) (*xdr.ReviewRequestResult, error) {
	env, err := tx.Marshal()
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal transaction")
	}

	res, err := s.Submit(ctx, env, true, false)
	if err != nil {
		if serr, ok := err.(submit.TxFailure); ok {
			err = errors.From(err, serr.GetLoganFields())
		}
		return nil, errors.Wrap(err, "failed to submit transaction")
	}

	var txResult xdr.TransactionResult
	err = xdr.SafeUnmarshalBase64(res.Data.Attributes.ResultXdr, &txResult)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal transaction result")
	}

	return (*txResult.Result.Results)[0].Tr.ReviewRequestResult, nil
}

func (s *Reviewer) submitMultipleReviewsTx(ctx context.Context, tx *xdrbuild.Transaction) ([]xdr.ReviewRequestResult, error) {
	env, err := tx.Marshal()
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal transaction")
	}

	res, err := s.Submit(ctx, env, true, false)
	if err != nil {
		if serr, ok := err.(submit.TxFailure); ok {
			err = errors.From(err, serr.GetLoganFields())
		}
		return nil, errors.Wrap(err, "failed to submit transaction")
	}

	var txResult xdr.TransactionResult
	err = xdr.SafeUnmarshalBase64(res.Data.Attributes.ResultXdr, &txResult)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal transaction result")
	}

	results := make([]xdr.ReviewRequestResult, 0, len(txResult.Result.MustResults()))

	for _, reviewResult := range txResult.Result.MustResults() {
		results = append(results, reviewResult.MustTr().MustReviewRequestResult())
	}

	return results[:], nil
}
