/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	EVENT   ResourceType = "event"
	HISTORY ResourceType = "history"
)
