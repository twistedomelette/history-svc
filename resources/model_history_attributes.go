/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type HistoryAttributes struct {
	CurrencyAmount  uint64 `json:"currency_amount"`
	CurrencyPayment string `json:"currency_payment"`
	Recipient       string `json:"recipient"`
	Sender          string `json:"sender"`
	Status          string `json:"status"`
	TokenId         int64  `json:"token_id"`
	Type            int8   `json:"type"`
	UsdAmount       uint64 `json:"usd_amount"`
}
