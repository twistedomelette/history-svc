/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EventIds struct {
	Key
	Attributes EventIdsAttributes `json:"attributes"`
}
type EventIdsResponse struct {
	Data     EventIds `json:"data"`
	Included Included `json:"included"`
}

type EventIdsListResponse struct {
	Data     []EventIds `json:"data"`
	Included Included   `json:"included"`
	Links    *Links     `json:"links"`
}

// MustEventIds - returns EventIds from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustEventIds(key Key) *EventIds {
	var eventIds EventIds
	if c.tryFindEntry(key, &eventIds) {
		return &eventIds
	}
	return nil
}
