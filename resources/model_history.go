/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type History struct {
	Key
	Attributes HistoryAttributes `json:"attributes"`
}
type HistoryRequest struct {
	Data     History  `json:"data"`
	Included Included `json:"included"`
}

type HistoryListRequest struct {
	Data     []History `json:"data"`
	Included Included  `json:"included"`
	Links    *Links    `json:"links"`
}

// MustHistory - returns History from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustHistory(key Key) *History {
	var history History
	if c.tryFindEntry(key, &history) {
		return &history
	}
	return nil
}
