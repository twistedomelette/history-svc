/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type EventIdsAttributes struct {
	Ids    []int64 `json:"ids"`
	Status string  `json:"status"`
}
